//
//  ViewController.m
//  lab2
//
//  Created by Dmitry Savchenko on 9/20/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController
@synthesize AddressField;
@synthesize TimezoneHolder;
@synthesize TemperatureHolder;
@synthesize IconHolder;
@synthesize locationManager;
@synthesize spinner;
@synthesize WeatherIcon;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.locationManager = [[CLLocationManager alloc] init];
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.delegate = self;
    [locationManager startUpdatingLocation];
    
    isInitialized = FALSE;
}

- (void)viewDidUnload
{
    [self setTimezoneHolder:nil];
    [self setTemperatureHolder:nil];
    [self setIconHolder:nil];
    [self setAddressField:nil];
    [self setWeatherIcon:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}
 
- (void) RenderWeatherForLocation:(NSString *)latitude
         longitude:(NSString *)longitude {    
    NSString *urlString = [[NSString alloc] 
                           initWithFormat:@"https://api.forecast.io/forecast/81f1f3b959074877b4ec22183b457971/%@,%@?units=si",
                           latitude, longitude];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSData *data = [NSData dataWithContentsOfURL:url];
    NSError *error = nil;
    
    id response = [NSJSONSerialization JSONObjectWithData:data options:
                   NSJSONReadingMutableContainers error:&error]; 
    
    id currentWeather = [response objectForKey: @"currently"];
    
//    id timezone = [response objectForKey: @"timezone"];
    id currentTemperature = [currentWeather valueForKey: @"apparentTemperature"];
    id currentIcon = [currentWeather objectForKey: @"icon"];
    id currentSummary = [currentWeather objectForKey: @"summary"];
    
    UIImage *img = [UIImage imageNamed:currentIcon];
    [WeatherIcon setImage:img];
    
//    TimezoneHolder.text = timezone;
    TemperatureHolder.text = [NSString stringWithFormat:@"%@", currentTemperature];
    IconHolder.text = currentSummary;
    
    [self loadLocationName:latitude longitude:longitude];
}

#pragma mark -
#pragma mark CLLocationManagerDelegate

-(void)locationManager:(CLLocationManager *)manager
   didUpdateToLocation:(CLLocation *)newLocation
          fromLocation:(CLLocation *)oldLocation
{
    lastLatitude = [[NSString alloc] 
                                 initWithFormat:@"%g", 
                                 newLocation.coordinate.latitude];
    
    lastLongitude = [[NSString alloc] 
                                  initWithFormat:@"%g",
                                  newLocation.coordinate.longitude];
 
    if (!isInitialized) {
        NSLog(@"%@, %@", lastLatitude, lastLongitude);
        
        isInitialized = TRUE;
        
        [self RenderWeatherForLocation:lastLatitude longitude:lastLongitude];
    }
}

- (IBAction)WeatherButtonClicked:(id)sender {
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    
    [geocoder geocodeAddressString:AddressField.text
                 completionHandler:^(NSArray *placemarks, NSError *error) {
                     
                     dispatch_async(dispatch_get_main_queue(),^ {
                         // do stuff with placemarks on the main thread
                         CLPlacemark *firstPlace = [placemarks objectAtIndex:0];
                         
                         CLLocationCoordinate2D firstLocation = [[firstPlace location] coordinate];
                         
                         [self RenderWeatherForLocation:[NSString stringWithFormat:@"%f", firstLocation.latitude] longitude:[NSString stringWithFormat:@"%f", firstLocation.longitude]];
                         
                         if (placemarks.count == 0) {
                             UIAlertView *alert = [[UIAlertView alloc] init];
                             alert.title = @"No places were found.";
                             [alert addButtonWithTitle:@"OK"];
                             [alert show];
                         }
                     });
                 }];
}

- (void)loadLocationName:(NSString *)latitude
               longitude:(NSString *)longitude {
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    
    CLLocation *location = [[CLLocation alloc] initWithLatitude:[latitude floatValue] longitude:[longitude floatValue]];

    [geocoder reverseGeocodeLocation:location
                 completionHandler:^(NSArray *placemarks, NSError *error) {
                     
                     dispatch_async(dispatch_get_main_queue(),^ {
                         // do stuff with placemarks on the main thread
                         CLPlacemark *firstPlace = [placemarks objectAtIndex:0];
                         
                         TimezoneHolder.text = [[firstPlace addressDictionary] objectForKey:@"City"];
                     });
                 }];
}

@end
