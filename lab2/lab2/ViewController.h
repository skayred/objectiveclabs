//
//  ViewController.h
//  lab2
//
//  Created by Dmitry Savchenko on 9/20/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#include <CoreLocation/CLLocationManagerDelegate.h>
#include <CoreLocation/CLLocation.h>
#include <CoreLocation/CLLocationManager.h>
#include <CoreLocation/CLGeocoder.h>
#include <CoreLocation/CLPlacemark.h>
#include <MapKit/MKReverseGeocoder.h>

@interface ViewController : UIViewController<CLLocationManagerDelegate> {
    CLLocationManager *locationManager;
    
    NSString *lastLatitude;
    NSString *lastLongitude;
    
    BOOL isInitialized;
}
- (IBAction)WeatherButtonClicked:(id)sender;
- (void)loadLocationName:(NSString *)latitude
               longitude:(NSString *)longitude;
@property (strong, nonatomic) IBOutlet UILabel *TimezoneHolder;
@property (strong, nonatomic) IBOutlet UILabel *TemperatureHolder;
@property (strong, nonatomic) IBOutlet UILabel *IconHolder;

@property (strong, nonatomic) CLLocationManager *locationManager;

@property (strong, nonatomic) NSString *lastLatitude;
@property (strong, nonatomic) NSString *lastLongitude;
@property (strong, nonatomic) IBOutlet UITextField *AddressField;
@property (strong, nonatomic) UIActivityIndicatorView *spinner;
@property (strong, nonatomic) IBOutlet UIImageView *WeatherIcon;

@end
