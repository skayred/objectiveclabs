#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@protocol UserLocationDelegate <NSObject>

@required
- (void)locationUpdate:(CLLocation *)location; 
- (void)locationError:(NSString *)errorMsg;

@end

@interface UserLocation : NSObject <CLLocationManagerDelegate> {
    
    CLLocationManager *locationManager;
    __weak id<UserLocationDelegate>delegate;
    
}

@property (nonatomic,strong) CLLocationManager *locationManager;
@property (nonatomic,weak) id<UserLocationDelegate>delegate;

-(id)initUserLocation;
-(void)getCurrentLocationOfUser;

@end