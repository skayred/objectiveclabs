#import "UserLocation.h"

@implementation UserLocation

@synthesize locationManager;
@synthesize delegate;

-(id)initUserLocation 
{
    if (self == [super init]) {
        self.locationManager = [[CLLocationManager alloc]init];
        self.locationManager.delegate = self;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    }
    return self;
}

-(void)getCurrentLocationOfUser {
    
    self.locationManager = [[CLLocationManager alloc]init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    
    if ([CLLocationManager locationServicesEnabled]) {
        //[self.locationManager startMonitoringSignificantLocationChanges];
        [self performSelector:@selector(startLocationUpdate) withObject:nil afterDelay:2.0];
    }
    else {
        if ([delegate respondsToSelector:@selector(locationError:)]) {
            [self.delegate locationError:@"Please Turn On Location Services in Settings To Retrive User's Current Location"];
        }
        else {
        }
    }
}

-(void)startLocationUpdate
{
    [self.locationManager startMonitoringSignificantLocationChanges];
}

#pragma mark CLLocationManagerDelegate

-(void)locationManager:(CLLocationManager *)manager
   didUpdateToLocation:(CLLocation *)newLocation
          fromLocation:(CLLocation *)oldLocation
{
    if ([delegate respondsToSelector:@selector(locationUpdate:)]) {
        [delegate locationUpdate:newLocation];
    }
}

-(void)locationManager:(CLLocationManager *)manager 
      didFailWithError:(NSError *)error
{
    if ([delegate respondsToSelector:@selector(locationError:)]) {
        [delegate locationError:@"Some Error Occured While Retriving Users's Location"];
    }
    else {
    }
}

@end