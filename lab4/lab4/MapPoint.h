//
//  MapPoint.h
//  lab4
//
//  Created by Dmitry Savchenko on 10/9/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MapPoint : NSObject

@property (nonatomic, retain) NSString *placeName;
@property (nonatomic) double placeLat;
@property (nonatomic) double placeLon;

@end
