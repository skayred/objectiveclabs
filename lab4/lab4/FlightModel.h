//
//  FlightModel.h
//  lab4
//
//  Created by Dmitry Savchenko on 10/9/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Flight.h"
#import "SQLiteDatabase.h"

@interface FlightModel : NSObject

- (Flight*) findFlight:(double) firstLat firstLon:(double)firstLon secondLat:(double)secondLat secondLon:(double)secondLon;

@end
