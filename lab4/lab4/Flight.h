//
//  Flight.h
//  lab4
//
//  Created by Dmitry Savchenko on 10/9/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MapPoint.h"

@interface Flight : NSObject

@property (nonatomic, retain) MapPoint *from;
@property (nonatomic, retain) MapPoint *to;
@property (nonatomic) double price;

@end
