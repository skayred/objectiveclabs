//
//  ViewController.h
//  lab4
//
//  Created by Dmitry Savchenko on 10/7/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "FlightModel.h"

@interface ViewController :  UIViewController <MKMapViewDelegate> {
    IBOutlet MKMapView *mapView;
    
    MKPointAnnotation* startPoint;
    MKPointAnnotation* endPoint;
    
    double startPointLat;
    double startPointLon;
    double endPointLat;
    double endPointLon;
    
    FlightModel* model;
}
@property (strong, nonatomic) IBOutlet MKMapView *mapView;
- (IBAction)findPressed:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *priceLabel;

@end
