//
//  FlightModel.m
//  lab4
//
//  Created by Dmitry Savchenko on 10/9/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "FlightModel.h"

@implementation FlightModel

- (Flight*) findFlight:(double) firstLat firstLon:(double)firstLon secondLat:(double)secondLat secondLon:(double)secondLon {    
    SQLiteDatabase *database = [[SQLiteDatabase alloc] initWithPath:[[NSBundle mainBundle] pathForResource:@"flights" ofType:@"sqlite"]];
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM prices ORDER BY ABS(a_lat - %f) + ABS(a_lon - %f) + ABS(b_lat - %f) + ABS(b_lon - %f) ASC", firstLat, firstLon, secondLat, secondLon];
    NSArray *queryResult = [database performQuery:query];
    
    for (NSArray *row in queryResult) {
        NSString *fromName = [row objectAtIndex:5];
        double fromLat = [[row objectAtIndex:0] floatValue];
        double fromLon = [[row objectAtIndex:1] floatValue];
        NSString *toName = [row objectAtIndex:6];
        double toLat = [[row objectAtIndex:2] floatValue];
        double toLon = [[row objectAtIndex:3] floatValue];
        
        double price = [[row objectAtIndex:4] floatValue];
        
        Flight* result = [Flight new];
        
        MapPoint* fromPoint = [MapPoint new];
        MapPoint* toPoint = [MapPoint new];
        
        [fromPoint setPlaceLat:fromLat];
        [fromPoint setPlaceLon:fromLon];
        [fromPoint setPlaceName:fromName];
        
        [toPoint setPlaceLat:toLat];
        [toPoint setPlaceLon:toLon];
        [toPoint setPlaceName:toName];
        
        [result setFrom:fromPoint];
        [result setTo:toPoint];
        
        [result setPrice:price];
        
        return result;
    }    
}

@end
