//
//  ViewController.m
//  lab4
//
//  Created by Dmitry Savchenko on 10/7/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController
@synthesize priceLabel;
@synthesize mapView;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    model = [FlightModel new];
    
    self.mapView.delegate = self;
    
    // Add an annotation
    startPoint = [[MKPointAnnotation alloc] init];
    startPoint.coordinate = CLLocationCoordinate2DMake(0, 0);
    startPoint.title = @"Start point";
    
    [mapView setCenterCoordinate:CLLocationCoordinate2DMake(5, 5)];
    
    endPoint = [[MKPointAnnotation alloc] init];
    endPoint.coordinate = CLLocationCoordinate2DMake(10, 10);
    endPoint.title = @"End point";
    
    [self.mapView addAnnotation:startPoint];
    [self.mapView addAnnotation:endPoint];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    static NSString *reuseId = @"pin";
    MKPinAnnotationView *pav = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:reuseId];
    if (pav == nil)
    {
        pav = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:reuseId];
        pav.draggable = YES;
        pav.canShowCallout = YES;
    }
    else
    {
        pav.annotation = annotation;
    }
    
    if([annotation.title isEqualToString:@"Start point"])
    {
        pav.animatesDrop=YES;
        pav.pinColor=MKPinAnnotationColorGreen;
    } else if([annotation.title isEqualToString:@"End point"])
    {
        pav.animatesDrop=YES;
        pav.pinColor=MKPinAnnotationColorRed;
    }
    
    return pav;
}

- (void)viewDidUnload
{
    [self setMapView:nil];
    [self setPriceLabel:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (IBAction)findPressed:(id)sender {
    Flight* result = [model findFlight:startPoint.coordinate.latitude firstLon:startPoint.coordinate.longitude secondLat:endPoint.coordinate.latitude secondLon:endPoint.coordinate.longitude];
    
    [startPoint setCoordinate:CLLocationCoordinate2DMake(result.from.placeLat, result.from.placeLon)];
    [startPoint setTitle:result.from.placeName];
    
    [endPoint setCoordinate:CLLocationCoordinate2DMake(result.to.placeLat, result.to.placeLon)];
    [endPoint setTitle:result.to.placeName];
    
    priceLabel.text = [NSString stringWithFormat:@"%.0f p.", result.price];
}
@end
