//
//  ViewController.h
//  lab3
//
//  Created by Dmitry Savchenko on 10/1/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController {
    CGPoint lastPoint;
    CGFloat red;
    CGFloat green;
    CGFloat blue;
    CGFloat brush;
    CGFloat opacity;
    BOOL mouseSwiped;
}
- (IBAction)eraserPressed:(id)sender;
- (IBAction)savePressed:(id)sender;
@property (strong, nonatomic) IBOutlet UISlider *lineWidth;
- (IBAction)pensilPressed:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *tempDrawImage;
@property (strong, nonatomic) IBOutlet UIImageView *mainImage;
@end