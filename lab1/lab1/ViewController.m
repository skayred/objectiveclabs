//
//  ViewController.m
//  lab1
//
//  Created by Dmitry Savchenko on 9/7/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController
@synthesize InputText;
@synthesize OutputText;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [self setInputText:nil];
    [self setOutputText:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (IBAction)CalculateCallback:(id)sender {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    
    [InputText.text enumerateSubstringsInRange:NSMakeRange(0, InputText.text.length) 
                                       options:NSStringEnumerationByWords 
                                    usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) { 
                                        if ([dic objectForKey:substring] == nil) {
                                            [dic setObject:[NSNumber numberWithInt:NSIntegerMax] forKey:substring];
                                        } else {
                                            int value = [[dic objectForKey:substring] integerValue] - 1;
                                            
                                            [dic setObject:[NSNumber numberWithInt:value] forKey:substring];
                                        }
                                        } ];
    
    NSArray *freqArray;
    freqArray = [dic keysSortedByValueUsingComparator:^(id obj1, id obj2) {        
        return [obj1 compare:obj2];        
    }];
    
    NSMutableString *buffer = [NSMutableString stringWithString:@""];
    int counter = 0;
    
    for (id element in freqArray) {
        if (counter == 5) {
            break;
        }
        
        [buffer appendString:element];
        [buffer appendString:@"\n"];
        
        counter++;
    }
    
    OutputText.text = buffer;
}
@end
