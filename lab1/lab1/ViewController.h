//
//  ViewController.h
//  lab1
//
//  Created by Dmitry Savchenko on 9/7/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
- (IBAction)CalculateCallback:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *InputText;
@property (weak, nonatomic) IBOutlet UILabel *OutputText;

@end
